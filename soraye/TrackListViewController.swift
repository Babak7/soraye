//
//  TrackListAndMediaPlayerViewController.swift
//  soraye
//
//  Created by babak on 29/06/16.
//  Copyright © 2016 babak. All rights reserved.
//


import UIKit

class TrackListViewController: UIViewController, UITableViewDelegate, UITableViewDataSource{
    
    var player: MP3Player?
    var timer:NSTimer?
    var isImageViewShowing:Bool?
    var songs:[String] = [String]()
    var albumName: String!
    var artistName: String!
    //var cameFromNowPlaying = false
    
    @IBOutlet weak var trackTable: UITableView!
    
    @IBOutlet var mainView: UIView!
//    @IBOutlet weak var mediaPlayerView: UIView!
//    
//    @IBOutlet weak var progressSliderBar: UISlider!
//
//    @IBOutlet weak var trackTime: UILabel!
//    @IBOutlet weak var trackNameLabel: UILabel!
//
//    @IBOutlet weak var contentView: UIView!
//    @IBOutlet weak var lyricsView: UITextView!
//    @IBOutlet weak var imageView: UIImageView!
    let tapRec = UITapGestureRecognizer()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NowPlayingBarButton.title = "Now Playing"
        
        let fileReader = FileReader(artisName: artistName)
        songs = fileReader.getAlbumTrackNames(albumName)
        
        if songs.count == 0 {
            print(fileReader.currentFolder)
        }
      
    }
    

    override func viewWillAppear(animated: Bool){
        super.viewWillAppear(true)
        
        prepareNavBarButtons()

        animateTable()
        
    }
    func prepareNavBarButtons() {
        if PlayerInfo.sharedInstance.mplayer != nil {
            NowPlayingBarButton.enabled = true
        } 
    }
    
    @IBOutlet weak var NowPlayingBarButton: UIBarButtonItem!
    @IBAction func NowPlayingBarButton(sender: UIBarButtonItem) {
        self.performSegueWithIdentifier("toMediaPlayerViewController", sender: nil)
    }
    
    

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return songs.count
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        let cell = UITableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "trackCell")
        cell.accessoryType = UITableViewCellAccessoryType.DisclosureIndicator
        cell.textLabel?.text = songs[indexPath.row]
        
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        self.performSegueWithIdentifier("toMediaPlayerViewController", sender: tableView)
    }

    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "toMediaPlayerViewController"{
            if let VC = segue.destinationViewController as? MediaPlayerViewController {
                if sender is UITableView {
                    let indexPath: NSIndexPath = trackTable.indexPathForSelectedRow!
                    VC.row = indexPath.row
                    VC.albumName = albumName
                    VC.artistName = artistName
                    

                } else {
                    VC.player = PlayerInfo.sharedInstance.mplayer
                }

                    
                    VC.title = "Player"
                    let backItem = UIBarButtonItem()
                    backItem.title = ""
                    navigationItem.backBarButtonItem = backItem
                
            }
        }
        
    }

    override func viewDidDisappear(animated: Bool) {
        
    }
    
    func animateTable() {
        trackTable.tableFooterView = UIView(frame: CGRect.zero)
        trackTable.reloadData()
        
        let cells = trackTable.visibleCells
        let tableHeight: CGFloat = trackTable.bounds.size.height
        
        for i in cells {
            let cell: UITableViewCell = i as UITableViewCell
            cell.transform = CGAffineTransformMakeTranslation(0, tableHeight)
        }
        
        var index = 0
        
        for a in cells {
            let cell: UITableViewCell = a as UITableViewCell
            UIView.animateWithDuration(1.5, delay: 0.05 * Double(index), usingSpringWithDamping: 0.8, initialSpringVelocity: 0, options: [], animations: {
                cell.transform = CGAffineTransformMakeTranslation(0, 0);
                }, completion: nil)
            
            index += 1
        }
    }
    
    
}

