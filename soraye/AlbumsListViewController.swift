//
//  FirstViewController.swift
//  soraye
//
//  Created by babak on 26/06/16.
//  Copyright © 2016 babak. All rights reserved.
//

import UIKit

class AlbumsListViewController: UIViewController, UITableViewDelegate, UITableViewDataSource{
    var player: MP3Player?
    var albums:[String] = [String]()
    @IBOutlet weak var albumTable: UITableView!
    var artistName: String!
    var albumName: String!
    @IBOutlet weak var NowPlayingBarButton: UIBarButtonItem!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //artistName = "Shamloo"
        
        albums = FileReader(artisName: artistName).readFolders()
        
        //print(albums)
        
        // Do any additional setup after loading the view, typically from a nib.
    }
    override func viewDidAppear(animated: Bool) {
        prepareNavBarButtons()
    }
    
    @IBAction func NowPlayingBarButton(sender: UIBarButtonItem) {
        //nowPlayingFlag = true
        performSegueWithIdentifier("toMediaPlayerViewController", sender: nil)
    }
    func prepareNavBarButtons() {
        if PlayerInfo.sharedInstance.mplayer != nil {
            NowPlayingBarButton.enabled = true
        } 
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return albums.count
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "cell")
        cell.accessoryType = UITableViewCellAccessoryType.DisclosureIndicator
        cell.textLabel?.text = albums[indexPath.row]
        
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        albumName = albums[indexPath.row]
        self.performSegueWithIdentifier("toTrackListViewController", sender: tableView)
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        if segue.identifier == "toTrackListViewController"{
            if let VC = segue.destinationViewController as? TrackListViewController{
                
                    VC.albumName = albumName
                    VC.artistName = artistName
                    VC.title = albumName
        
                    let backItem = UIBarButtonItem()
                    backItem.title = ""
                    navigationItem.backBarButtonItem = backItem
                                  
            
            }
            
            
        }
        if segue.identifier == "toMediaPlayerViewController" {
            if let VC = segue.destinationViewController as? MediaPlayerViewController {
                VC.player = PlayerInfo.sharedInstance.mplayer
                let backItem = UIBarButtonItem()
                backItem.title = ""
                navigationItem.backBarButtonItem = backItem
            }
        }
        
    }
   

}

