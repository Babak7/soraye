//
//  MediaPlayerViewController.swift
//  soraye
//
//  Created by babak on 26/06/16.
//  Copyright © 2016 babak. All rights reserved.
//

import UIKit
import AVFoundation
import MediaPlayer

class MediaPlayerViewController: UIViewController {
    var player: MP3Player?
    var timer:NSTimer?
    @IBOutlet weak var playerToolbar: UIToolbar!
    var albumName:String!
    var artistName: String!
    var row:Int!
    var isImageViewShowing:Bool?
    @IBOutlet weak var progressBar: UIProgressView!
    @IBOutlet weak var progressSliderBar: UISlider!
    @IBOutlet weak var trackNameLabel: UILabel!
    @IBOutlet weak var trackTime: UILabel!
    
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var lyricsView: UITextView!
    let tapRec = UITapGestureRecognizer()
    

    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Player"
        
        if player == nil {
            mediaPlayerViewDidLoad(false)
        } else {
            artistName = PlayerInfo.sharedInstance.artistName
            albumName = PlayerInfo.sharedInstance.albumName
            mediaPlayerViewDidLoad(true)
            startTimer()
        }
   
    }
    
    override func viewDidAppear(animated: Bool) {
    
        
    }
    
    func mediaPlayerViewDidLoad(nowPlayingFlag: Bool) {
        
        preparePlayerButtons(nowPlayingFlag)
        
        if player == nil || nowPlayingFlag == false {
            if PlayerInfo.sharedInstance.mplayer?.isPlaying() == true {
                PlayerInfo.sharedInstance.mplayer?.stop()
                
            }
            player = MP3Player()
            player?.prepareAlbumTracks(artistName, albumName: albumName, row: row)
            
        }
        updateViews()
        
        
        let ratio : CGFloat = CGFloat (0.5)
        let thumbImage : UIImage = UIImage(named: "sliderThumbImage.png")!
        let size = CGSizeMake( thumbImage.size.width * ratio * 0.20, thumbImage.size.height * ratio )
        progressSliderBar.setThumbImage( thumbImage.imageWithImage(scaledToSize: size), forState: UIControlState.Normal )
        
        
        
        //---- Prepare gesture recognizer ----------------------------------
        tapRec.addTarget(self, action: #selector(MediaPlayerViewController.tappedContentView))
        tapRec.numberOfTapsRequired = 1
        let parentViewTabRec = UITapGestureRecognizer(target: self, action: #selector(MediaPlayerViewController.tappedContentView))
        // ---------------------------------------------------------------
        
        contentView.addGestureRecognizer(parentViewTabRec)
        imageView.addGestureRecognizer(tapRec)
        lyricsView.addGestureRecognizer(tapRec)
        isImageViewShowing = true
        
    }
    

    
    
    func preparePlayerButtons(playButtonFlag: Bool) {
        if playButtonFlag {
            playPauseButton.setTitle("\u{f144}", forState: .Normal)
        } else {
            playPauseButton.setTitle("\u{f28b}", forState: .Normal)
        }
        
        forwardButton.setTitle("\u{f050}", forState: .Normal)
        rewindButton.setTitle("\u{f049}", forState: .Normal)
    }

    
    func updateViews(){
        // Prepare track iformation -----------
        player?.fetchTrackInfo()
        setTrackName()
        setAlbumImage()
        setLyrics()
        
        trackTime.text = player?.getCurrentTimeAsString()
        if let progress = player?.getProgress() {
            progressSliderBar.value = progress
            
        }
        if (player?.isPlaying()) == true {
            playPauseButton.setTitle("\u{f28b}", forState: .Normal) // Show Play Button
        }
        else {
            playPauseButton.setTitle("\u{f144}", forState: .Normal)  // Show Pause Button
        }
        
        
    }

    

    func setTrackName(){
        trackNameLabel.text = player?.getCurrentTrackName()
    }
    func setAlbumImage() {
        imageView.image = player?.getArtworkOfTrack()
    }
    func setLyrics() {
        lyricsView.text = player?.getLyricsOfTrack()
        lyricsView.textAlignment = .Right
        lyricsView.scrollRangeToVisible(NSMakeRange(0, 0))
    }
    
    func tappedContentView() {
        
        if isImageViewShowing == true {
            isImageViewShowing = false
            UIView.transitionFromView(imageView, toView: lyricsView, duration: 1.0, options: [UIViewAnimationOptions.TransitionFlipFromRight, UIViewAnimationOptions.ShowHideTransitionViews], completion: nil)

        } else {
            isImageViewShowing = true
            UIView.transitionFromView(lyricsView, toView: imageView, duration: 1.0, options: [UIViewAnimationOptions.TransitionFlipFromRight, UIViewAnimationOptions.ShowHideTransitionViews], completion: nil)
        }
    }
    
    

    @IBOutlet weak var playPauseButton: UIButton!
    @IBAction func playPauseButton(sender: UIButton) {
        
        if (player?.isPlaying()) == true {
            
            player?.pause()
            updateViews()
            timer?.invalidate()
            //playPauseButton.setTitle("\u{f04b}", forState: .Normal)
            playPauseButton.setTitle("\u{f144}", forState: .Normal) // Show Play Button
        
        }
        else {
            player?.getLyricsOfTrack()
            player?.play()
            startTimer()
            //playPauseButton.setTitle("\u{f04c}", forState: .Normal)
            playPauseButton.setTitle("\u{f28b}", forState: .Normal)  // Show Pause Button
        }
        
    }
    
   
        func fastForwardLongPress() {
        player?.fastForward()
    }
    
    @IBOutlet weak var forwardButton: UIButton!
    @IBAction func forwardButton(sender: UIButton) {
        player?.nextSong(false)
        startTimer()
    }
    
    @IBOutlet weak var rewindButton: UIButton!
    @IBAction func rewindButton(sender: UIButton) {
        player?.previousSong()
        startTimer()
    }
    
    
    func fastBackwardLongPress() {
        player?.fastbackward()
    }
    
   
    
    @IBOutlet var slider: UISlider!
    @IBAction func sliderController(sender: UISlider) {
        player?.setVolume(slider.value)
        
    }
    
    @IBAction func progressSliderBar(sender: UISlider) {
        if (player?.isPlaying()) == true {
            player?.pause()
            player?.setCurrentTime(NSTimeInterval(sender.value))
            trackTime.text = player?.getCurrentTimeAsString()
            player?.play()
        
        }
        else {
            
            player?.setCurrentTime(NSTimeInterval(sender.value))
            trackTime.text = player?.getCurrentTimeAsString()
        }
        

    }
    func startTimer(){
        timer = NSTimer.scheduledTimerWithTimeInterval(1.0, target: self, selector: #selector(MediaPlayerViewController.updateViewsWithTimer(_:)), userInfo: nil, repeats: true)
    }
    
    func updateViewsWithTimer(theTimer: NSTimer){
        updateViews()
    }
 
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
//    override func viewDidDisappear(animated: Bool) {
//        player?.stop()
//    }
    
    override func viewWillDisappear(animated: Bool)
    {
        PlayerInfo.sharedInstance.mplayer = player
        PlayerInfo.sharedInstance.artistName = artistName
        PlayerInfo.sharedInstance.albumName = albumName
    }
    

    
}
