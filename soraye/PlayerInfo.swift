//
//  PlayerInfo.swift
//  soraye
//
//  Created by babak on 14/07/16.
//  Copyright © 2016 babak. All rights reserved.
//

import Foundation



class PlayerInfo {
    class var sharedInstance: PlayerInfo {
        struct Static {
            static var instance: PlayerInfo?
            static var token: dispatch_once_t = 0
        }
        
        dispatch_once(&Static.token) {
            Static.instance = PlayerInfo()
        }
        
        return Static.instance!
    }
    
    
    var mplayer : MP3Player! //Some String
    
    var albumName: String!
    var artistName: String!
    
}