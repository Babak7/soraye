//
//  Mp3Player.swift
//  soraye
//
//  Created by babak on 27/06/16.
//  Copyright © 2016 babak. All rights reserved.
//

import UIKit
import AVFoundation
import MediaPlayer

class MP3Player: NSObject, AVAudioPlayerDelegate {
    
    var player:AVAudioPlayer?
    var currentTrackIndex = 0
    var tracks:[String] = [String]()
    var albumName:String?
    var trackName:String?
    var artistName:String?
    var albumImage: UIImage?
    var lyrics: String = "Not Found!"
    var asset: AVURLAsset?
    override init(){
        super.init()
        
        // ---- Activate control center buttons ----
        let commandCenter = MPRemoteCommandCenter.sharedCommandCenter()
        commandCenter.pauseCommand.enabled = true
        commandCenter.pauseCommand.addTarget(self, action: #selector(MP3Player.pause))
        commandCenter.playCommand.enabled = true
        commandCenter.playCommand.addTarget(self, action: #selector(MP3Player.play))
        commandCenter.nextTrackCommand.enabled = true
        commandCenter.nextTrackCommand.addTarget(self, action: #selector(MP3Player.nextSong(_:)))
        commandCenter.previousTrackCommand.enabled = true
        commandCenter.previousTrackCommand.addTarget(self, action: #selector(MP3Player.previousSong))
        
        UIApplication.sharedApplication().beginReceivingRemoteControlEvents()
        _ = try? AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback)
        // -----------------------------------------

    }
    
    func updateRemotePlayerInfo(){
        
         _ = try? AVAudioSession.sharedInstance().setActive(true)
        MPNowPlayingInfoCenter.defaultCenter().nowPlayingInfo = [
            MPMediaItemPropertyArtist: getTrackArtistName(),
            MPMediaItemPropertyTitle: getCurrentTrackName(),
            //MPMediaItemPropertyArtwork: (player?.getArtworkForTrack())!,
            MPMediaItemPropertyPlaybackDuration: getCurrentTrackDuration(),
            MPNowPlayingInfoPropertyPlaybackRate: NSNumber(integer: 1),
            MPNowPlayingInfoPropertyElapsedPlaybackTime: (player?.currentTime)!
        ]
        
        if player?.playing == false {
            DeactivateCommandCenter()
        }
        
    }

    func prepareAlbumTracks(artistName: String, albumName: String, row: Int){
        //print("prepare = "+albumName)
        currentTrackIndex = row
        tracks = FileReader(artisName: artistName).getAlbumTracks(albumName)
        queueTrack();
    }
    func queueTrack(){
        if (player != nil) {
            player = nil
        }
        
//        var error:NSError?
        
        let url = NSURL.fileURLWithPath(tracks[currentTrackIndex] as String)
        do {
           try player = AVAudioPlayer(contentsOfURL: url)
            
        }
        catch{
            print("error")
        }
        
        player?.delegate = self
        player?.prepareToPlay()
        
    }
    func play() {
        
        if player?.playing == false {
            player?.play()
            updateRemotePlayerInfo()
        }
    }
    func isPlaying() -> Bool {
        if player?.playing == true {
            return true
        }
        else{
            return false
        }

    }
    func pause(){
        if player?.playing == true {
            player?.pause()
            
           DeactivateCommandCenter()

        }
    }
    
    func DeactivateCommandCenter() {
        // ------ Perform pause on Contorl Center -----
        MPNowPlayingInfoCenter.defaultCenter().nowPlayingInfo?[MPNowPlayingInfoPropertyElapsedPlaybackTime] = player?.currentTime
        MPNowPlayingInfoCenter.defaultCenter().nowPlayingInfo?[MPNowPlayingInfoPropertyPlaybackRate] = 0.0
        _ = try? AVAudioSession.sharedInstance().setActive(false)
        // -------------------------------------------
    }
    
    func stop(){
        if player?.playing == true {
            player?.stop()
            player?.currentTime = 0
        }
    }
    
    func nextSong(songFinishedPlaying:Bool){
        var playerWasPlaying = false
        
        if player?.playing == true {
            player?.stop()
            playerWasPlaying = true
        }
        
        currentTrackIndex += 1
        if currentTrackIndex >= tracks.count {
            currentTrackIndex = 0
        }
        queueTrack()
        
        if playerWasPlaying || songFinishedPlaying {
            player?.play()
        }
        fetchTrackInfo()
        updateRemotePlayerInfo()
    }
    
    func previousSong(){
        var playerWasPlaying = false
        if player?.playing == true {
            player?.stop()
            playerWasPlaying = true
        }
        currentTrackIndex -= 1
        if currentTrackIndex < 0 {
            currentTrackIndex = tracks.count - 1
        }
        
        queueTrack()
        if playerWasPlaying {
            player?.play()
        }
        fetchTrackInfo()
        updateRemotePlayerInfo()
    }
    
    func fastForward() {
        if var time = player?.currentTime {
            time += 3.0; // forward 3 secs
            if (time > player?.duration)
            {
                nextSong(false)
            }
            else {
                player?.currentTime = time;
            }

        }
    }
    func fastbackward() {
        if var time = player?.currentTime {
            time -= 3.0; // forward 5 secs
            if (time < 0)
            {
                previousSong()
            }
            else {
                player?.currentTime = time;
            }
            
        }
        
    }
    
    func getCurrentTimeAsString() -> String {
        var seconds = 0
        var minutes = 0
        if let time = player?.currentTime {
            seconds = Int(time) % 60
            minutes = (Int(time) / 60) % 60
        }
        return String(format: "%0.2d:%0.2d",minutes,seconds)
    }
    
    func getProgress()->Float{
        var theCurrentTime = 0.0
        var theCurrentDuration = 0.0
        if let currentTime = player?.currentTime, duration = player?.duration {
            theCurrentTime = currentTime
            theCurrentDuration = duration
        }
        return Float(theCurrentTime / theCurrentDuration)
    }

    func setCurrentTime(value: NSTimeInterval) {
        player?.currentTime = value * (player?.duration)!
    }
    
    func getCurrentTrackDuration() -> NSTimeInterval {
        return (player?.duration)!
    }
    
    func setVolume(volume:Float){
        player?.volume = volume
    }
    
    func audioPlayerDidFinishPlaying(player: AVAudioPlayer, successfully flag: Bool){
        if flag == true {
            nextSong(true)
        }
    }
    
    
    func fetchTrackInfo() {
        let audioPathUrl = NSURL.fileURLWithPath(tracks[currentTrackIndex] as String)
        let playerItem = AVPlayerItem(URL: audioPathUrl)
        
        let metadataList = playerItem.asset.metadata
        for item in metadataList {
            //print(item)
            let lyricsKey = String(item.key!)
            if (lyricsKey.rangeOfString("ULT") != nil) || (lyricsKey.rangeOfString("USLT") != nil) {
                lyrics = (item.value as? String)!
                
            }
            
            guard let key = item.commonKey, let value = item.value else{
                continue
            }
            
            switch key {
            case "title" : trackName = value as? String
            case "artist": artistName = value as? String
            case "artwork": albumImage = UIImage(data: item.dataValue!)
                
            default:
                continue
            }
        }
        //print(trackName!)
        
    
    }
    
    func getCurrentTrackName() -> String {
        return trackName!
    }
    
    func getTrackArtistName() -> String {
        return artistName!
    }
    
    func getArtworkOfTrack() -> UIImage  {
        fetchTrackInfo()
        return albumImage!
    }
    
    func getLyricsOfTrack() -> String {
        fetchTrackInfo()
        return lyrics
    }
    
    
    
}