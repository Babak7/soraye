//
//  SecondViewController.swift
//  soraye
//
//  Created by babak on 26/06/16.
//  Copyright © 2016 babak. All rights reserved.
//

import UIKit



class ChooseArtistViewController: UIViewController {
    var player: MP3Player?
    var jiggling: Bool = false
    var artistName: String!
    @IBOutlet weak var ShamlooButton: UIButton!
   
    @IBOutlet weak var AkhavanSalesButton: UIButton!
    
    @IBOutlet weak var NowPlayingBarButton: UIBarButtonItem!
    
    @IBOutlet weak var mainView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        MakeCircleButtons(ShamlooButton, title: "Shamloo")
        MakeCircleButtons(AkhavanSalesButton, title: "AkhavanSales")
        
        // Do any additional setup after loading the view, typically from a nib.
        
    }
    
    override func viewDidAppear(animated: Bool) {
        
        if jiggling == false {
            for view in mainView.subviews as [UIView] {
                if let button = view as? UIButton {
                    moveButton(button)
                    smoothJiggle(button)
                }
            }
            
            
        }
        prepareNavBarButtons()
        
    }
    
    func MakeCircleButtons(button: UIButton, title: String){
        
//        let xPosition : CGFloat = CGFloat( arc4random_uniform(UInt32(mainView.bounds.width) - 20))
//        let yPosition : CGFloat = CGFloat( arc4random_uniform(UInt32(mainView.bounds.height) - 50))
//        let widthAndHeight : CGFloat = CGFloat( arc4random_uniform(UInt32(40)))+110
//        print(xPosition)
//        let button = UIButton(frame: CGRect(x: xPosition, y: yPosition, width: widthAndHeight, height: widthAndHeight))
        
        //let widthAndHeight : CGFloat = CGFloat( arc4random_uniform(UInt32(20)))+130
        button.bounds.size.width = 150
        button.bounds.size.height = 150
        print(button.bounds.size.width)
        
//        let r = CGFloat(Float(arc4random()) / Float(UINT32_MAX))
//        print(r)
        button.layer.cornerRadius = 0.5 * button.bounds.size.width
        //button.setImage(UIImage(named:"thumbsUp.png"), forState: .Normal)
        button.addTarget(self, action: #selector(artistButtonPressed(_:)), forControlEvents: .TouchUpInside)
        //button.layer.cornerRadius = 50
        button.clipsToBounds = true
        button.layer.borderWidth = 1
        button.layer.backgroundColor = UIColor.lightGrayColor().CGColor
        button.layer.borderColor = UIColor.blackColor().CGColor
        button.setTitle(title, forState: .Normal)
        

    }
    
    @IBAction func NowPlayingBarButton(sender: UIBarButtonItem) {
        performSegueWithIdentifier("toMediaPlayerViewController", sender: nil)
    }
    func prepareNavBarButtons() {
        if PlayerInfo.sharedInstance.mplayer != nil {
            NowPlayingBarButton.enabled = true
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func artistButtonPressed(btn: UIButton) {
        artistName = btn.titleLabel?.text!
        performSegueWithIdentifier("ToAlbumsListViewController", sender: nil)
        
    }
    
    func moveButton(button: UIButton) {
        let animation = CABasicAnimation(keyPath: "position")
        animation.duration = 2.0
        animation.repeatCount = Float.infinity
        animation.autoreverses = true
        let rand_x0 : CGFloat = CGFloat( arc4random_uniform(10))+1
        // set yPosition to be a random number between 1.0 and 10.0
        let rand_y0 : CGFloat = CGFloat( arc4random_uniform(10))+1
        animation.fromValue = NSValue(CGPoint: CGPointMake(button.center.x + rand_x0, button.center.y + rand_y0 ))
        let rand_x1 : CGFloat = CGFloat( arc4random_uniform(10))-10
        let rand_y1 : CGFloat = CGFloat( arc4random_uniform(10))-10
        animation.toValue = NSValue(CGPoint: CGPointMake(button.center.x + rand_x1, button.center.y - rand_y1))
        
        button.layer.addAnimation(animation, forKey: "position")
    }
    
    func smoothJiggle(button :UIButton) {
        
        let degrees: CGFloat = 3.0
        let animation = CAKeyframeAnimation(keyPath: "transform.rotation.z")
        animation.duration = 3.0
        animation.cumulative = true
        animation.repeatCount = Float.infinity
        animation.values = [0.0,
                            degreesToRadians(-degrees) * 0.25,
                            0.0,
                            degreesToRadians(degrees) * 0.5,
                            0.0,
                            degreesToRadians(-degrees),
                            0.0,
                            degreesToRadians(degrees),
                            0.0,
                            degreesToRadians(-degrees) * 0.5,
                            0.0,
                            degreesToRadians(degrees) * 0.25,
                            0.0]
        animation.fillMode = kCAFillModeForwards;
        animation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionLinear)
        animation.removedOnCompletion = true
        animation.autoreverses = true
        button.layer.addAnimation(animation, forKey: "transform.rotation.z")
        jiggling = true
    }
    func degreesToRadians(degreeNumber: CGFloat) -> Double {
        return Double(degreeNumber) * M_PI / 180
    }
    
    func stopMovingButtons() {
        jiggling = false
        if jiggling == false {
            for view in mainView.subviews as [UIView] {
                if let button = view as? UIButton {
                    button.layer.removeAllAnimations()
                    button.transform = CGAffineTransformIdentity
                    button.layer.anchorPoint = CGPointMake(0.5, 0.5)                }
            }
            
            
        }

        
    }
    
    func offStage(amount: CGFloat) -> CGAffineTransform {
        return CGAffineTransformMakeTranslation(amount, 0)
    }
    
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "ToAlbumsListViewController"{
            if let VC = segue.destinationViewController as? AlbumsListViewController {
                
                VC.artistName = artistName
                let backItem = UIBarButtonItem()
                backItem.title = ""
                navigationItem.backBarButtonItem = backItem
                
            }
        }
        if segue.identifier == "toMediaPlayerViewController" {
            if let VC = segue.destinationViewController as? MediaPlayerViewController {
                VC.player = PlayerInfo.sharedInstance.mplayer
                let backItem = UIBarButtonItem()
                backItem.title = ""
                navigationItem.backBarButtonItem = backItem
            }
        }
    }
    
    override func viewDidDisappear(animated: Bool) {
       
        stopMovingButtons()
    }
    
    
    
}
