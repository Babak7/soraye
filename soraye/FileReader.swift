//
//  FileReader.swift
//  soraye
//
//  Created by babak on 27/06/16.
//  Copyright © 2016 babak. All rights reserved.
//

import UIKit
import AVFoundation

extension Dictionary where Value : Equatable {
    func KeysForValue(val : Value) -> [Key] {
        return self.filter { $1 == val }.map { $0.0 }
    }
}

class FileReader: NSObject {
    var tracks:[String] = [String]()
    var folderMapNames = [String: String]()
    var currentFolder: String!
    
    var artist = "Shamloo"
    init(artisName: String?) {
        if let temp = artisName {
            artist = temp
        }
    }
    func readFolders() -> [String]{
        let fm = NSFileManager.defaultManager()
        let path = NSBundle.mainBundle().resourcePath!
        
        
        
        let artistFolder = path + "/\(artist)"
        folderMapNames = readjson("\(artist)/albums", jsonKey: "albums")
        
        var farsiFolderNames = [String]()
        let items = try! fm.contentsOfDirectoryAtPath(artistFolder)

        for item in items {
            if let folderName = folderMapNames[item] {
                farsiFolderNames.append(folderName)
            }
            else {
                folderMapNames.removeValueForKey(item)
            }
           
        }
        //print(farsiFolderNames)
        return farsiFolderNames
    }
    
    func getAlbumTracks(albumName: String) -> [String] {

        folderMapNames = readjson("\(artist)/albums", jsonKey: "albums")
        let englishAlbumName:String = folderMapNames.KeysForValue(albumName)[0]
        let directory = "\(artist)/" + englishAlbumName
        currentFolder = directory
        return  NSBundle.mainBundle().pathsForResourcesOfType("mp3", inDirectory: directory)
    }
    
    func getAlbumTrackNames(albumName: String) -> [String] {
        tracks = getAlbumTracks(albumName)
        var trackNames:[String] = [String]()
        for (_, value) in tracks.enumerate(){
            trackNames.append(getTrackNameForTableRow(value))
           //trackNames.append(value.componentsSeparatedByString("/").last!.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet()))
        }
        
        return trackNames
    }
    
    func getTrackNameForTableRow(audioPath: String) -> String {
        let audioPathUrl = NSURL.fileURLWithPath(audioPath as String)
        let playerItem = AVPlayerItem(URL: audioPathUrl)
    
        var trackName: String?
        let metadataList = playerItem.asset.metadata
        
        for item in metadataList {
            
            guard let key = item.commonKey, let value = item.value else{
                continue
            }

            switch key {
            case "title" : trackName = value as? String
            default:
                continue
            }
        }
        return trackName!
    }
    
    func readjson(fileName: String, jsonKey: String) -> [String: String] {
        let url = NSBundle.mainBundle().URLForResource(fileName, withExtension: "json")
        let data = NSData(contentsOfURL: url!)
        var listOfresults = [String: String]()
        do {
            let object = try NSJSONSerialization.JSONObjectWithData(data!, options: .AllowFragments)
            if let dictionary = object as? [String: AnyObject] {
                //readJSONObject(dictionary, key: jsonKey)
                listOfresults = (dictionary[jsonKey] as? [String: String])!
               
            }
        } catch {
            print("Error in open serialization")
        }
        return listOfresults
    }
    
    func readJSONObject(object: [String: AnyObject], key: String ) -> [String: String] {
        var listOfresults = [String: String]()
        if let value = object[key] as? [String: String] {
            listOfresults = value
        }
        else {
            print(key)
        }
        
        return listOfresults
    }
    
    
}